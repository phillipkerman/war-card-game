class Constants  {

    static get CARD_WIDTH() { return 45 };
    static get CARD_HEIGHT() { return 51 };


    static get SUIT_CLUBS() { return "clubs" };
    static get SUIT_SPADES() { return  "spades" };
    static get SUIT_HEARTS() { return  "hearts" };
    static get SUIT_DIAMONDS() { return  "diamonds" };

    static get FACE_TWO() { return  2 };
    static get FACE_THREE() { return  3 };
    static get FACE_FOUR() { return  4 };
    static get FACE_FIVE() { return  5 };
    static get FACE_SIX() { return  6 };
    static get FACE_SEVEN() { return  7 };
    static get FACE_EIGHT() { return  8 };
    static get FACE_NINE() { return  9 };
    static get FACE_TEN() { return  10 };
    static get FACE_JACK() { return  11 };
    static get FACE_QUEEN() { return  12 };
    static get FACE_KING() { return  13 };
    static get FACE_ACE() { return  14 };



    //order based on sprite sheet columns
    static get FACE_LOOKUP() { return [Constants.FACE_ACE,
                                        Constants.FACE_TWO,
                                        Constants.FACE_THREE,
                                        Constants.FACE_FOUR,
                                        Constants.FACE_FIVE,
                                        Constants.FACE_SIX,
                                        Constants.FACE_SEVEN,
                                        Constants.FACE_EIGHT,
                                        Constants.FACE_NINE,
                                        Constants.FACE_TEN,
                                        Constants.FACE_JACK,
                                        Constants.FACE_QUEEN,
                                        Constants.FACE_KING ] }

    //order based on sprite sheet rows
    static get SUIT_LOOKUP() { return  [Constants.SUIT_CLUBS,
                                        Constants.SUIT_SPADES,
                                        Constants.SUIT_HEARTS,
                                        Constants.SUIT_DIAMONDS ]}
    
    //for english version of cards
    static faceValueToWord(face){
        return ["na","na","two","three","four","five","six","seven","eight","nine","ten","jack","queen","king","ace"][face]
    }
   
}

export {Constants as Constants};



