import {Constants} from './Constants';
import {CardTableRenderer} from './CardTableRenderer';
import {GameStateManager} from './GameStateManager';
import {DeckVO} from './datatypes/DeckVO';

let cardTableContainerElement;//where a new <table> will appear at every step of game
let nextButtonElement;//hook for user to advance the game
let startGameButtonElement;//hook for user to start a new game
let numPlayersSelectElement;//select element to specify number of players
let numPlayersLabelElement;//select element label
let promptElement;//for user guide
let autoRunButtonElement;//hook to auto advance 'next' button
let intervalID = -1;//to turn on/off auto-run

//html render helper
const cardTableRenderer = new CardTableRenderer();
//game state
const gameState = new GameStateManager();


const init = () => {
  //a few hooks to the UI
  cardTableContainerElement = document.querySelector('#cardtablecontainer');
  nextButtonElement = document.querySelector('#nextbutton');
  autoRunButtonElement = document.querySelector('#autorunbutton');
  startGameButtonElement = document.querySelector('#startgamebutton');
  numPlayersSelectElement = document.querySelector('#numplayers');
  numPlayersLabelElement = document.querySelector('#numplayerslabel');
  promptElement = document.querySelector('#prompt');
  promptElement.innerHTML = "";

  //listener for start game
  startGameButtonElement.addEventListener('click', clickStartGame);

  //listener for advancing through game
  nextButtonElement.addEventListener('click', clickNextButton);
  autoRunButtonElement.addEventListener('click', clickAutoRunButton);

  gameState.startOver();
  updateUI();
}

const updateUI = () => {
  //display some text, show/hide elements
  startGameButtonElement.innerText =  !gameState.inGame ? "Confirm" : "Start Over";
  
  numPlayersSelectElement.className = gameState.inGame ? 'disabled-control': null;
  numPlayersLabelElement.className = gameState.inGame ? 'hidden-control' : null;
  
  autoRunButtonElement.className = !gameState.gameActive ? 'hidden-control': null;

  nextButtonElement.className = !gameState.gameActive ? 'hidden-control': null;
  nextButtonElement.innerText =  gameState.getNextButtonLabel();
  
  //clear card table
  while(cardTableContainerElement.firstChild){
    cardTableContainerElement.removeChild(cardTableContainerElement.firstChild);
  }

  if ( gameState.inGame ){
    //draw new card table
    cardTableContainerElement.appendChild(cardTableRenderer.makeNewCardTable(gameState.getPlayerData()));
  }

  //display prompt (only using "Game Over")
  promptElement.innerHTML = gameState.getStatusPrompt();
  
  
}


const clickStartGame = () => {
  if ( gameState.inGame ){
    gameState.startOver();
  }else{
    gameState.startGame(numPlayersSelectElement.value, new DeckVO());
  }
  //stop auto run if running
  if ( intervalID !== -1 ) clickAutoRunButton();
  updateUI(); 
}

const clickNextButton = () => {
  gameState.nextMove();
  updateUI(); 
}
const clickAutoRunButton = () => {
  if ( intervalID !== -1 ){
    clearInterval(intervalID);
    autoRunButtonElement.innerText = "Auto Run";
    intervalID = -1;
  }else{
    intervalID = setInterval( ()=>{  
                  gameState.nextMove();
                  updateUI(); 
                }, 100);
    autoRunButtonElement.innerText = "Stop Auto Run"
  }
}

//kick start
init();