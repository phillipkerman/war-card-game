import {Constants} from '../Constants';
class CardVO {
    
    constructor(face, suit){
        this.face = face; 
        this.suit = suit;   
        this.value = face;
    }
    toString(){
        return `${Constants.faceValueToWord(this.face)} of ${this.suit}`;
    }

}


export {CardVO as CardVO};