import {expect} from 'chai';
import {CardVO} from './CardVO';
import {PlayerVO} from './PlayerVO';
import {Constants} from '../Constants';

describe('PlayerVO...', ()=>{

    let cards, extraCards, tempPlayer;
    beforeEach(()=>{
        cards = [ ];
        cards.push(new CardVO(Constants.FACE_ACE,Constants.SUIT_CLUBS));
        cards.push(new CardVO(Constants.FACE_KING,Constants.SUIT_DIAMONDS));

        extraCards = [];
        extraCards.push(new CardVO(Constants.FACE_NINE,Constants.SUIT_SPADES));
        extraCards.push(new CardVO(Constants.FACE_QUEEN,Constants.SUIT_HEARTS));

        tempPlayer = new PlayerVO(cards, 'temp player');
    })

    it( 'can maintain cardsRemaining state', ()=>{
        expect(tempPlayer.cardsRemaining.length).to.equal(2);
    })

    it( 'can remember name', ()=>{
        expect(tempPlayer.name).to.equal('temp player');
    })

    it( 'can win cards', ()=>{
        tempPlayer.winCards(extraCards);
        expect(tempPlayer.cardsRemaining.length).to.equal(4);
    })
    
    it( 'can play first card without going bust', ()=>{
        let firstCardStr = tempPlayer.cardsRemaining[0].toString();
        expect(tempPlayer.getCardsToPlay(1).toString()).to.equal(firstCardStr);
        expect(tempPlayer.status).to.not.equal(PlayerVO.STATUS_BUST);
    })

    it( 'can play as many cards as they have but then go bust', ()=>{
        let cardCount = tempPlayer.cardsRemaining.length;
        expect(tempPlayer.getCardsToPlay(cardCount+1).length).to.equal(cardCount);
        expect(tempPlayer.status).to.equal(PlayerVO.STATUS_BUST);
    })

    it( 'can remember last card played', ()=>{
        expect(tempPlayer.getCardsToPlay(1)[0].toString()).to.equal(tempPlayer.currentCard.toString());
    })
    
    it( 'can remember last card played, even when multiples played', ()=>{
        let someCards = tempPlayer.getCardsToPlay(500);
        expect(someCards[someCards.length-1].toString()).to.equal(tempPlayer.currentCard.toString());
    })

});