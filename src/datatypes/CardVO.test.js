import {expect} from 'chai';
import {CardVO} from './CardVO';
import {Constants} from '../Constants';

describe('CardVO...', ()=>{

    it( 'can generate a nice string', ()=>{
        let tempCard = new CardVO(Constants.FACE_EIGHT, Constants.SUIT_DIAMONDS);
        expect(tempCard.toString()).to.equal("eight of diamonds");
    })


});