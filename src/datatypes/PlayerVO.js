import {Constants} from '../Constants';
import {CardVO} from './CardVO';

class PlayerVO {
    
    static get STATUS_BUST(){ return "bust" }
    static get STATUS_ACTIVE(){ return "active" }
    static get STATUS_NOT_IN_WAR(){ return "not in war" }
    static get STATUS_WINNER(){ return "winner" }
    static get STATUS_LOSER(){ return "loser" }
        
    //this.cardsRemaining (array of CardVOs)
    //this.name (string)
    //this.status (string)
    //this.lastCardPlayed (CardVO)
    constructor(cardArray, playername){
        this.name = playername;
        this.cardsRemaining = cardArray;
        this.status = PlayerVO.STATUS_ACTIVE;
    }

    get numCards(){
        return this.cardsRemaining.length;
    }
    get currentCard(){
        return this.lastCardPlayed;
    }
   
    winCards(cards){
        this.cardsRemaining = this.cardsRemaining.concat(cards);
    }


    getCardsToPlay(howMany){
        this.status = PlayerVO.STATUS_ACTIVE;
        
        if ( howMany > this.cardsRemaining.length ){
            howMany = this.cardsRemaining.length;
            this.status = PlayerVO.STATUS_BUST;
        }
        //track the last card played
        this.lastCardPlayed = this.cardsRemaining[howMany-1];
        return this.cardsRemaining.splice(0,howMany);    
    }


   

}

export {PlayerVO as PlayerVO};