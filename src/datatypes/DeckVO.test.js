import {expect} from 'chai';
import {DeckVO} from './DeckVO';
import {CardVO} from './CardVO';
import {Constants} from '../Constants';

describe('DeckVO...', ()=>{

    it( 'can create a 52 card deck', ()=>{
        let tempDeck = new DeckVO();
        expect(tempDeck.length).to.equal(52);
    })

    it( 'can create a stacked deck, high cards last', ()=>{
        let tempDeck = new DeckVO();
        tempDeck.stackDeck();
        //last one should be ace
        let lastCard = tempDeck.cards[tempDeck.length-1];
        expect(lastCard.value).to.equal(Constants.FACE_ACE);

        //fifth from the last should be king
        let nearLastCard = tempDeck.cards[tempDeck.length-5];
        expect(nearLastCard.value).to.equal(Constants.FACE_KING);
    })

    it( 'can create 52 unique cards', ()=>{
        let hash = {};
        let tempDeck = new DeckVO();
        while(tempDeck.length>0){
            let topCard = tempDeck.take(1)[0].toString();
            expect(hash[topCard]).to.equal(undefined);
            hash[topCard] = true;
        }
        expect(Object.keys(hash).length).to.equal(52);
    })
   

});