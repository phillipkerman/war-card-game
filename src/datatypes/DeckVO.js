import {Constants} from '../Constants';
import {CardVO} from './CardVO';

class DeckVO {
    
    constructor(){
        this.cards = [];
        //fill the deck
        const howManySets = 1;//can do bigger decks, but 1 is fine for now
        (new Array(howManySets).fill(0)).forEach( () => {
            Constants.FACE_LOOKUP.forEach((face) => {
                Constants.SUIT_LOOKUP.forEach((suit) => {
                    this.cards.push( new CardVO(face,suit) );
                });
            });
        });
        this.shuffle();
    }
    
    //to make testing easier (put high value cards later in deck)
    stackDeck(how){
        this.cards.sort( (a,b) => {
            if ( a.value>b.value){
                return 1;
            }else{
                return -1;
            }
        })
    }
    shuffle(){
        this.cards.sort( (a,b) => (Math.random() >0.5 ? 1 : -1) );
    }
    
    get length(){
        return this.cards.length;
    } 

    toString(){
        return this.cards.map(card=>card.toString()).join("\n");
    }

    take( howMany ){
        return this.cards.splice(0,howMany);
    }

}

export {DeckVO as DeckVO};