import {Constants} from './Constants';
import {PlayerVO}  from './datatypes/PlayerVO'
class CardTableRenderer {
    
    constructor(){
        
    }

    makeCardElement (cardVO) {

        var el = document.createElement('div');
        el.className = 'card-style';
        
        let col = Constants.FACE_LOOKUP.indexOf(cardVO.face);
        let row = Constants.SUIT_LOOKUP.indexOf(cardVO.suit);

        const x = Constants.CARD_WIDTH * -(col);
        const y = Constants.CARD_HEIGHT * -(row);
        
        el.style['background-position'] = `${x}px ${y}px`;
        
        return el;
      }
      
    makeNewCardTable(playerVOArray){
       
        let table = document.createElement('table');

        const arrayMatchingPlayerCount = new Array(playerVOArray.length).fill(0);
        
        //do one for each player:
        arrayMatchingPlayerCount.forEach( (na, index) => {
            let thisCol = document.createElement('col');
            
            switch(playerVOArray[index].status){
                case PlayerVO.STATUS_WINNER:
                    thisCol.className = 'cell-winner';
                    break;
                case PlayerVO.STATUS_LOSER:
                    thisCol.className = 'cell-loser';
                    break;
                default:
                    thisCol.className = 'cell-inactive';
                    break;
            }
            
            table.appendChild(thisCol);
        });
        
        //row for playernames:
        let playerNamesRow = document.createElement('tr');
        arrayMatchingPlayerCount.forEach( (na, index) => {
            let thisData = document.createElement('td');
            thisData.innerHTML = playerVOArray[index].name;
            playerNamesRow.appendChild(thisData);
        } );
        table.appendChild(playerNamesRow);

        let playerStatsRow = document.createElement('tr');
        arrayMatchingPlayerCount.forEach( (na, index) => {
            let thisData = document.createElement('td');
            thisData.innerHTML = `${playerVOArray[index].numCards} cards remain`;
            playerStatsRow.appendChild(thisData);
        } );
        table.appendChild(playerStatsRow);

        let playerDeckRow = document.createElement('tr');
        arrayMatchingPlayerCount.forEach( (na, index) => {
            let thisData = document.createElement('td');
            if ( playerVOArray[index].currentCard ){
                thisData.appendChild(this.makeCardElement(playerVOArray[index].currentCard));
            }
            playerDeckRow.appendChild(thisData);
        } );
        table.appendChild(playerDeckRow);

        return table;

    }


    

       
}

export {CardTableRenderer as CardTableRenderer};