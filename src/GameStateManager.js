import {Constants} from './Constants';
import {CardVO} from './datatypes/CardVO';
import {DeckVO} from './datatypes/DeckVO';
import {PlayerVO} from './datatypes/PlayerVO';
class GameStateManager {
    
    get STEP_0_SELECT_PLAYERS(){ return "SELECT PLAYERS" }
    get STEP_0_BEGIN_GAME(){ return "BEGIN GAME" }
    get STEP_1_TURN_COMPLETE(){ return "TURN COMPLETE" }
    get STEP_1_SHOW_DOWN(){ return "SHOW DOWN" }
    get STEP_2_GAME_OVER(){ return "GAME OVER" }

    //getters for main index
    get inGame(){
       return this.currentStep !== this.STEP_0_SELECT_PLAYERS;
    }
    get gameActive(){
        return this.currentStep !== this.STEP_0_SELECT_PLAYERS &&
                this.currentStep !== this.STEP_2_GAME_OVER;
    }
    getPlayerData(){
        return this.playerData;
    }
    getNextButtonLabel(){
        return this.currentStep === this.STEP_1_SHOW_DOWN ?
        "War" : this.currentStep === this.STEP_0_BEGIN_GAME ?
        "Start" : "Next";
    }
    getStatusPrompt(){
        if (this.currentStep === this.STEP_2_GAME_OVER){
            return "Game Over";
        } else{
            return "";
        }    
    }

    //return an array of players with matching status
    static playersWithMatchingStatus (players,status) {
        return players.filter(player=>player.status===status);
    }
    //return an array of cards gathered from each player
    static getCardsFromPlayers (players,howMany) {
        return players.reduce((prev,player) =>{
            prev = prev.concat(player.getCardsToPlay(howMany));
            return prev;
        }, []);
    }

 
    constructor(){
    
    }

    startOver(){
        this.currentStep = this.STEP_0_SELECT_PLAYERS;
    }

    startGame(numberOfPlayers, deckVO){
        this.currentStep = this.STEP_0_BEGIN_GAME;
       
        const cardsEachPlayer = Math.floor(deckVO.length/numberOfPlayers);

        this.playerData = [];
        for(let i=0; i<numberOfPlayers; i++){
          this.playerData.push( new PlayerVO(deckVO.take(cardsEachPlayer), `Player ${i+1}`));
        }
        //initialize the pot
        this.thePot = [];
    }

    doPlay(){
        //add to the pot by getting one card from each player:
        this.thePot = this.thePot.concat( GameStateManager.getCardsFromPlayers (this.playerData, 1) );
        
        //judge
        this.judgeResults();
    }
 
    
    doWar(){
        //from winners...
        let justWinners = GameStateManager.playersWithMatchingStatus(this.playerData, PlayerVO.STATUS_WINNER);
       
        //get 4 cards each to add to the pot
        this.thePot = this.thePot.concat( GameStateManager.getCardsFromPlayers (justWinners, 4) );

        //judge
        this.judgeResults();
    }

    judgeResults(){
        //figure out highest
        let activePlayers = GameStateManager.playersWithMatchingStatus(this.playerData, 
                                                                    PlayerVO.STATUS_ACTIVE);
         //find the highest value among active players
         let highestValue = activePlayers.reduce((prev, player) =>{
              return Math.max(player.currentCard.value, prev);
          }, 0);
  
          //get a list of winners (those with card matching highest value)
          let winners = activePlayers.reduce((prev, player) =>{
              if ( player.currentCard.value === highestValue ){
                  player.status = PlayerVO.STATUS_WINNER;
                  prev.push(player);
              }else{
                  player.status = PlayerVO.STATUS_LOSER;
              }
              return prev;
          }, []);
          
          //if exactly 1 winner exists...
          if ( winners.length === 1 ){
              this.currentStep = this.STEP_1_TURN_COMPLETE;
              //give cards in play to winner
              winners[0].winCards(this.thePot);
              
              //clear pot
              this.thePot = [];
          }else{
              //deactivate (visually) ones not in war
              this.playerData.map(player=>{
                  if ( player.status !== PlayerVO.STATUS_WINNER &&
                      player.status !== PlayerVO.STATUS_BUST ){
                  
                      player.status = PlayerVO.STATUS_NOT_IN_WAR;
              
                  }
              })
              //if zero winners ("winner" could have gone bust first), end turn...otherwise, war
              this.currentStep = winners.length === 0 ? this.STEP_1_TURN_COMPLETE : this.STEP_1_SHOW_DOWN;
          }
  
          //check if there's just one player waith any cards
          let playersWithCards = this.playerData.filter(player=>player.cardsRemaining.length>0);
          if ( playersWithCards.length === 1 ){
              //give them the cards (in case there was a war)
              playersWithCards[0].winCards(this.thePot);
              //end game
              this.currentStep = this.STEP_2_GAME_OVER;
          }
    }

    nextMove(){
        switch(this.currentStep){

            case this.STEP_0_BEGIN_GAME:
            case this.STEP_1_TURN_COMPLETE:
                this.doPlay();
                break;

            case this.STEP_1_SHOW_DOWN:
                this.doWar();
                break;

        }
        
        
    }

   
}

export {GameStateManager as GameStateManager};