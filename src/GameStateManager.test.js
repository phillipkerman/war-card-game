import {expect} from 'chai';
import {GameStateManager} from './GameStateManager';
import {PlayerVO} from './datatypes/PlayerVO';
import {DeckVO} from './datatypes/DeckVO';
import {Constants} from './Constants';


describe('GameStateMananger utilities...', ()=>{

    let gameStateManager
    beforeEach(()=>{
        gameStateManager = new GameStateManager();
        gameStateManager.startGame(4, new DeckVO());
    })
    it( 'can filter players with status active', ()=>{
        let activeList = GameStateManager.playersWithMatchingStatus(gameStateManager.getPlayerData(),
                                                            PlayerVO.STATUS_ACTIVE);
        expect(activeList.length).to.equal(4);

    })
    it( 'can filter players with status winner', () =>{
        gameStateManager.getPlayerData()[0].status = PlayerVO.STATUS_WINNER;
        let winnerList = GameStateManager.playersWithMatchingStatus(gameStateManager.getPlayerData(),
                                                            PlayerVO.STATUS_WINNER);
        expect(winnerList.length).to.equal(1);
    })
    
    it( 'can get cards from each player', () =>{  
        const numCardsPlayer1Before = gameStateManager.getPlayerData()[0].cardsRemaining.length;
        const pot = GameStateManager.getCardsFromPlayers(gameStateManager.getPlayerData(), 4);
        expect(pot.length).to.equal(4*4);//four players, four cards each
        const numCardsPlayer1After = gameStateManager.getPlayerData()[0].cardsRemaining.length;
        expect(numCardsPlayer1Before-numCardsPlayer1After).to.equal(4);
    })
});

describe('GameStateMananger can perform expected outcomes with stacked deck', ()=>{
    
        let gameStateManager
        beforeEach(()=>{
            gameStateManager = new GameStateManager();
        })

        it ( 'will be game over in just a few turns', ()=>{
            
            const stackedDeck = new DeckVO();
            stackedDeck.stackDeck();
            gameStateManager.startGame(13, stackedDeck);

            gameStateManager.nextMove();
            gameStateManager.nextMove();
            gameStateManager.nextMove();
            gameStateManager.nextMove();
            expect(gameStateManager.currentStep).to.equal(gameStateManager.STEP_2_GAME_OVER);

        })




});




 