# War Card Game
Requested code sample.

### Install

```
npm install
```
At worst, you may also need to have webpack and mocha installed:

```
npm install mocha webpack -g
```


### To build
```
npm run build
```
then open ./bin/index.html

### To run tests
```
npm run test
```

##Notes
To play, select number of players and click 'Confirm'.

Then, click 'Start' to deal the first hand. 

Continue by clicking 'Next' or 'War' (depending on outcome of current hand).

Edge cases:

- if a player has no cards remaining, they have gone bust and can't play anymore.
- if a player is in a war/showdown and they don't have enough cards (4, 3 extra plus the next one to flip over) they go bust, lose the cards they have and will not win the war.
- if all players in a war don't have enough cards to continue, their cards go into the pot for the next hand (and they're eliminated as they've gone bust).
