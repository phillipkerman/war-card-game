var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');
var webpack = require('webpack');
module.exports = {
    entry: './src/index.js',
    output: {
        path:  path.resolve(__dirname, 'bin'),  
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
     
    externals: {

    },
        
    plugins: [
        
            //move all the static files into build
            new CopyWebpackPlugin([{ from: 'src/_static/' }]) 
        
        ],

    module: {
        
        loaders: [
            {   test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }}
        ]
    }
}